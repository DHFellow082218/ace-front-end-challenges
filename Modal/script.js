class Item 
{
    constructor(name, price)
    {
        this.name       =       name; 
        this.price      =       price; 
    }
}


const table             =   document.querySelector('#table_item');
const modal             =   document.querySelector('#modal');

table.addEventListener("click", e => 
{
    if(e.target.nodeName == "BUTTON")
    {   
        modal.querySelector('#row_index').value =  (e.target.parentElement.parentElement.rowIndex - 1);
        modal.style.display                     =   'flex';   
        modal.querySelector('.modal').classList.add('modal-down'); 
    }
});

modal.addEventListener("click", e => 
{
    if(!e.target.nodeName == "BUTTON")
    {
        return; 
    }

    if(e.target.classList.contains('item-retain'))
    {
        
    }

    if(e.target.classList.contains('item-remove'))
    {
        const index         =       modal.querySelector('#row_index').value;
    
        table.querySelector('tbody').rows[index].classList.add('cross-out');

        setTimeout(() => 
        {
            table.querySelector('tbody').deleteRow(index);
        }, 
        1000
        );
       
    }

    modal.querySelector('.modal').classList.remove('modal-down'); 
    modal.querySelector('.modal').classList.add('modal-up'); 
    
    setTimeout(() => 
    {
        modal.style.display         =   'none';
        modal.querySelector('.modal').classList.remove('modal-up'); 
    }, 
    800
    ); 
})

const items     =   [
                        new Item("apple", "0.99"),
                        new Item("mango", "0.29"),
                        new Item("orange", "0.89"),
                        new Item("grapes", "0.79"),
                        new Item("guava", "0.59"),
                        new Item("knife", "2.99"),
                        new Item("frying pan", "2.99"),
                        new Item("toilet paper", "2.99"),
                        new Item("picnic basket", "5.99"),
                    ];




items.forEach((item, index) => 
{
    const row = table.querySelector('tbody').insertRow(0); 
    
    const cell0 = row.insertCell(0)
    const cell1 = row.insertCell(1)
    const cell2 = row.insertCell(2)

    const button = document.createElement("button"); 

    button.classList.add('btn', 'btn-danger');
    button.innerHTML = 'Remove'; 

    cell0.appendChild(button);
    cell1.innerHTML = item.name;
    cell2.innerHTML = item.price;
}) 