import Card from "./classes/Card.js";

const loader            =       document.getElementById('loader'); 
const hamburger         =       document.getElementById('hamburger'); 
const menu              =       document.getElementById('menu'); 
const links             =       document.getElementById('links'); 
const posts             =       document.getElementById('posts');

hamburger.addEventListener('click', ()  => 
{
    menu.classList.toggle('hidden');
});


const get_cards         =       async(resource)     =>
{
    const response      =       await   fetch(resource); 
    const data          =       await   response.json();
    
    return data; 
}  

console.log('test');

get_cards("../assets/js/data/post.json")
    .then(data  =>  
    {
        data.forEach(card       => 
        {
            new Card(card, posts);
        });

        loader.classList.add('animation-fade'); 

        setTimeout(() =>
        {
           loader.remove();
        }, 3000)
    })
    .catch(err  =>  console.log(err))

