export default class Card 
{
    constructor(data, container)
    {
        const   node            =       this.create(data); 
        this.append(node, container); 
    }

    create(data) 
    {
        const   node            =       document.createElement('aside'); 

        node.classList.add('card'); 

        node.innerHTML          =       `<header>
                                            <h2>
                                            ${data.title}
                                            </h2> 
                                        </header>
                                        <main>
                                            ${data.content}
                                        </main>
                                        <footer>
                                            <p>Posted on ${data.created_at} by ${data.user}</p>
                                        </footer>`;

        return node; 
    }

    append(node, container) 
    {
        container.insertAdjacentElement('afterbegin', node);
    }


    
}