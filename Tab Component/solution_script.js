const TAB_CONTENT = [
    {
      "label": "Description",
      "content": "The worlds fastest fidget spinner"
    },
    {
      "label": "Specs",
      "content": "Slime green, low friction ball bearings, with dual finger grips"
    },
    {
      "label": "Description",
      "content": "The worlds fastest fidget spinner"
    }
]
  
  class Tabs {
    constructor (el, tabbedContent = [], activeIndex = 0) {
      // Clear existing content
      el.innerHTML = ''
      // Setup internal properties
      this.activeIndex = activeIndex
      this.tabbedContent = tabbedContent
  
      // Create containing elements
      const nav = document.createElement('nav')
      const list = document.createElement('ul')
      list.className = 'tabs__list'
  
      // Handle click on the tabs
      this.handleClick = this.handleClick.bind(this)
      nav.addEventListener('click', this.handleClick)
  
      // Render the nav labels
      tabbedContent.forEach((tab, index) => list.insertAdjacentHTML('beforeend', `
        <li data-tab-index=${index} class="tabs__tab">
          ${tab.label}
        </li>
      `))
      nav.appendChild(list)
  
      // Render the content
      this.content = document.createElement('article')
      this.content.className = 'tabs__content'
      this.content.innerHTML = tabbedContent[activeIndex]
        && tabbedContent[activeIndex].content
  
      el.appendChild(nav)
      el.appendChild(this.content)
    }
  
    handleClick (e) {
      const i = e.target.dataset.tabIndex
      this.content.innerHTML = this.tabbedContent[i]
        && this.tabbedContent[i].content
    }
  }
  
  const node = document.getElementById('tabs')
  new Tabs(node, TAB_CONTENT)