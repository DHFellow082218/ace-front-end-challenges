const container                         =       document.querySelector('.container');

class TabComponent
{
    constructor(data, container)
    {
        this.node                       =       this.create_node(data)
        this.append_node(this.node, container)
    }

    create_node(data)
    {

        let tabs                        =       ''; 
        let content                     =       ''; 
        
        data.forEach((item, index)      =>  
        {
            tabs                        +=      `<button type="button" data-target="content_${index}">${item.label}</button>`; 
            content                     +=      `<section data-id="content_${index}">${item.content}</section>`;
        })

        const node                      =       document.createElement('div')

        node.className                  =       'tab'; 
        node.innerHTML                  =       `<header>
                                                    ${tabs}
                                                </header>
                                                <main>
                                                    ${content}
                                                </main>`;

        node.addEventListener('click', this.handle_event)

        node.querySelector('header').querySelector('button').classList.add("active");
        node.querySelector('main').querySelector('section').classList.add("active");

        return node;
    }

    handle_event(e) 
    {    
        if(e.target.tagName !== "BUTTON")
        {
            alert(e.target.parent());
            return; 
        }

      
        const target                    =       e.target.dataset.target;
        const root                      =       e.target.parentNode.parentNode;

        root.querySelectorAll('button').forEach(element => 
        {
            element.classList.remove('active');
        });

        e.target.classList.add('active')

        const sections                  =       root.querySelector('main').querySelectorAll('section');

        sections.forEach(content        =>      
        {
            content.classList.remove('active')

            if(content.dataset.id       ==      target)
            {
                content.classList.add('active')
            }
        });        
    }

    append_node(node, container) 
    {
        container.insertAdjacentElement('afterbegin', node) 
    }

}



const get_data      =       async()     => 
{
    const response  =       await fetch("./data.json")
    const data      =       await response.json() 

    if(response.status !== 200)
    {
        throw new Error('cannot fetch the data'); 
    }

    return data; 
}


get_data()
    .then(data => 
    {
        new TabComponent(data, container)
    })
    .catch(err => 
    {
        console.log(err);
    });