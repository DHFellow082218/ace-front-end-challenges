const main_content          =       document.getElementById("creets"); 

class Card
{
    constructor(container, data)
    {
        this.node           =           this.create_node(data); 
        this.append(container); 
    }

    create_node(data) 
    {

        const node          =    `<article class="card grid">
                                    <section class="image">
                                        <img src="${data.user.avatar}" alt="">
                                    </section>
                                    <section class="content">
                                        <header>
                                            <strong>
                                                @${data.user.username}                
                                            </strong>
                                            <span>
                                                &middot; ${data.created_at.slice(4, 10)}     
                                            </span>
                                        </header>
                                        <main>
                                            <p>
                                                ${data.text}
                                            </p>
                                        </main>
                                        <footer>
                                            <strong>
                                                Likes           
                                            </strong>
                                            <span>
                                                &middot; ${data.likes}
                                            </span>
                                        </footer>
                                    </section>
                                </article>`;

        return node; 
    }

    append(container) 
    {
        container.insertAdjacentHTML("afterBegin", this.node)
    }
}

/* const get_creets            =       (resource, callback)      => 
{
    const request           =       new XMLHttpRequest(); 

    request.addEventListener("readystatechange", ()   =>  
    {
        if(request.status === 200 && request.readyState === 4)
        {
            callback(undefined, JSON.parse(request.responseText));
        }
        else if(request.readyState === 4)
        {
            callback("Something Went Wrong", undefined);
        }
    });

    request.open("GET", resource);
    request.send(); 
}


get_creets(
    "https://www.acefrontend.com/c/critter/feed.json", 
    (err, data)     =>  
    {
        if(err)
        {
            console.log(err);
            return
        }

        data.feed.forEach(creek => 
        {
            new Card(main_content, creek);
        });  
    }
) */

fetch("https://www.acefrontend.com/c/critter/feed.json")
    .then(response => 
    {
        return response.json()
    })
    .then(data => 
    {
        data.feed.forEach(creek => 
        {
            new Card(main_content, creek);
        });  
    })
    .catch(err =>
    {
        alert(err);
    });

