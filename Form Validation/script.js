const form                      =           document.getElementById('form_register'); 

form.addEventListener('submit', (e) => 
{
    e.preventDefault() ;

    const form_data             =   new FormData(e.target); 
    const feedback              =   e.target.querySelectorAll('.feedback');

    let   errors                =   0; 
   

    console.log(e.target.name)

    const name                  =   form_data.get('_name'); 
    const email                 =   form_data.get('_email');
    const password              =   form_data.get('_password');
    const confirm_password      =   form_data.get('_confirm_password');


    console.table({name, email, password, confirm_password});

    feedback.forEach(field => 
    {
        field.innerText     =   '';
    })

    
    if(name.trim() === '')
    {
        feedback[0].innerText   =   "Name is Required";   
        errors++;
    }

    if(email.trim() === '')
    {
        feedback[1].innerText   =   "Email is Required";   
        errors++;
    }


    if(password.trim() === '')
    {
        feedback[2].innerText   =   "Password is Required";  
        errors++; 
    }
    
    if(password.trim().length <=   6)
    {
        feedback[2].innerText   =   "Password must be more than 6 characters";   
        errors++;
    }

    if(password !== confirm_password)
    {
        feedback[3].innerText   =   "Passwords Do NOT Match";   
        errors++;
    }

    if(errors >= 1)
    {
        return; 
    }

    alert('success');
})