<h1>
    Challenges from Ace Front End
</h1>

<ul>
    <li>Dropdown</li>
    <li>Modal</li>
    <li>Critter</li>
    <li>Form Validation</li>
    <li>Tab Component</li>
    <li>Holy Grail Layout</li>
</ul>

<a href="https://www.acefrontend.com/" target="_blank">
    Link to Website
</a>